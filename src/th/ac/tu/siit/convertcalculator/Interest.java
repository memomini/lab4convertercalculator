package th.ac.tu.siit.convertcalculator;

import java.util.Locale;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Interest extends Activity implements OnClickListener {
	
	float IntRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.btnConvertInterest);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSetting);
		b2.setOnClickListener(this);
		
		TextView tvRate = (TextView)findViewById(R.id.tvRate);
		IntRate = Float.parseFloat(tvRate.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.exchange, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.btnConvertInterest) {
			EditText dps = (EditText)findViewById(R.id.etIN);
			EditText yrs = (EditText)findViewById(R.id.year);
			TextView tvmoney = (TextView)findViewById(R.id.tvmoney);
			
			String res = "";
			try {
				float deposit = Float.parseFloat(dps.getText().toString());
				int years = Integer.parseInt(yrs.getText().toString());
				double money = deposit * ( Math.pow(1+(IntRate*0.01), years));
				res = String.format(Locale.getDefault(), "%.2f", money);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			tvmoney.setText(res);
		}
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("IntRate", IntRate);
			startActivityForResult(i, 9999); // 9999 = a request cide, it is a unique integer value for internally identifying
			// the return value
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			IntRate = data.getFloatExtra("IntRate", 0.03f);
			TextView tvRate = (TextView)findViewById(R.id.tvRate);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", IntRate));
		}
	}

	
}
